#ifndef HASH_HPP
#define HASH_HPP


struct Bucket;
struct Transaction;
template <class T> class LinkedList;

class Hash {
    private:
        int BUCKET;
        Bucket **table;

    public:
        Hash(int,int);
        LinkedList<Transaction*>* getUserIdTransactions(char*);
        bool searchTransaction(char*);
        void insertItem(char*,Transaction*);
        int hash(char*);
        ~Hash();
};

#endif