#ifndef TRANSACTION_HPP
#define TRANSACTION_HPP

#include <ctime>

struct Transaction {
    char* transactionId;
    char* senderId;
    char* receiverId;
    int amount;
    struct tm tmstamp;

    Transaction(char*,char*,char*,int,struct tm);
    ~Transaction();
};

#endif