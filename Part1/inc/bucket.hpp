#ifndef BUCKET_HPP
#define BUCKET_HPP

struct Transaction;
template <class T> class LinkedList;

struct Record {
    char *userId;
    LinkedList<Transaction*> *trList;
    Record(char *);
    ~Record();
};

struct Bucket {
    int bucketSize;
    Bucket *next;
    Record** records;
    int numOfRecords;  

    Bucket(int);
    LinkedList<Transaction*>* getUseridTransactions(char*);
    bool searchTransaction(char*);
    void insert(char*,Transaction*);
    ~Bucket();
};

#endif