#ifndef USER_HPP
#define USER_HPP

struct Bitcoin;
template <class T> class MyVector;

class User {
    private:
        char *userId;
        int balance;
        MyVector<Bitcoin*>* bitCoinList;
    
    public:
        User(char *);
        ~User();
        MyVector<Bitcoin*>* getBitCoinList();
        char* getUserId();
        int getBalance();
        void setBalance(int);
        void addBitCoin(Bitcoin*);
};

#endif