#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#define DELIM " "
#define TRANSACTION_DELIM ";"
#define RAND_LENGTH 5

class User;
class Hash;
struct Transaction;
struct Bitcoin;
template <class T> class MyVector;

bool isValidUserId(MyVector<User*>&, char*);
bool isValidBitCoin(MyVector<User*>&,User*,int);
bool isValidTransaction(MyVector<User*>&,Transaction*);
int walletStatus(MyVector<User*>&,char*);
int findTransactions(Hash&,char*);
bool isInPeriod(struct tm,struct tm,struct tm,int);
void swapStrings(char*, char*);
void depositBitCoins(User*,User*,int);
void printBitCoins(User*);
int existsBitCoin(MyVector<Bitcoin*>*,int);
bool afterLastDate(struct tm,struct tm);
bool requestTransaction(Hash&,Hash&,MyVector<Transaction*>&,MyVector<User*>&,char*);
void requestTransactions(Hash&,Hash&,MyVector<Transaction*>&,MyVector<User*>&,char*);
int intN(int);
char* randomString(int);

#endif