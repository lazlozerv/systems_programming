#include <cstdlib>
#include <cstring>
#include "../inc/bucket.hpp"
#include "../inc/linked_list.hpp"
#include "../inc/transaction.hpp"

#define RECORDSIZE sizeof(Record)

/*
 * Here we have all the hashtable's bucket functionality,
 * which is implemented like singly linked list with blocks
 */


Record::Record(char* id) {
    this->userId = (char*)malloc((strlen(id)+1) * sizeof(char));
    strcpy(userId,id);
    trList = new LinkedList<Transaction*>;
}

Record::~Record() {
    free(userId);
    delete trList;
}


Bucket::Bucket(int sz) {
    this->bucketSize = sz;
    this->numOfRecords = 0;
    this->next = NULL;
    this->records = new Record*[bucketSize/RECORDSIZE];
}


/*
 * Searching if transactionId exists already
 */
bool Bucket::searchTransaction(char *transactionId) {
    Bucket *temp = this;
    while(temp != NULL) {
        for(int i=0; i<temp->numOfRecords; i++) {
            Node<Transaction*> *tr = temp->records[i]->trList->begin();
            while(tr != NULL) {
                if(strcmp(tr->data->transactionId,transactionId) == 0) {
                    return true;
                }
                tr = tr->next;
            }
        }
        temp = temp->next;
    }
    return false;
}

/*
 * Here we search for the transactions of a specific user
 */
LinkedList<Transaction*>* Bucket::getUseridTransactions(char* id) {
    Bucket *temp = this;
    while(temp != NULL) {
        for(int i=0; i<temp->numOfRecords; i++) {
            if(strcmp(temp->records[i]->userId,id) == 0) {
                return temp->records[i]->trList;
            }
        }
        temp = temp->next;
    }
    return NULL;
}

/*
 * Here we insert a transaction into the bucket
 * If user exists already then we push the transaction into the list of transactions,
 * otherwise we make a new Record and if necessary a new Bucket first
 */
void Bucket::insert(char* id,Transaction *tr) {
    Bucket *last,*temp=this;
    while(temp!=NULL) {
        last = temp;
        for(int i=0; i<temp->numOfRecords; i++) {
            if(strcmp(temp->records[i]->userId,id) == 0) {
                temp->records[i]->trList->insert(tr);
                return ;
            }
        }

        temp = temp->next;
    }

    if(last->numOfRecords == last->bucketSize/RECORDSIZE) {
        last->next = new Bucket(bucketSize);
        last = last->next;
    }
    last->records[last->numOfRecords] = new Record(id);
    last->records[last->numOfRecords++]->trList->insert(tr);
}

//Here we just free the memory
Bucket::~Bucket() {
    if(this != NULL) {
        for(int i=0; i<this->numOfRecords; i++) {
            delete this->records[i];
        }
        delete []this->records;
        delete this->next;    
    }
}
