#include <iostream>
#include <cstdio>
#include <cstring>
#include "../inc/linked_list.hpp"
#include "../inc/hash.hpp"
#include "../inc/functions.hpp"
#include "../inc/transaction.hpp"
#include "../inc/bitcoin.hpp"
#include "../inc/user.hpp"
#include "../inc/myvector.hpp"


/*
 * Searches if there is a user already that has that userId
 */
bool isValidUserId(MyVector<User*>& vct,char *userId) {
    for(int i=0; i<vct.size(); i++) {
        if(strcmp(vct[i]->getUserId(),userId) == 0) {
            return false;
        }
    }
    return true;
}

/*
 * Searching all bitCoins of all up to that moment known users and checking 
 * if they already have a bitCoin with bitCoinId
 */
bool isValidBitCoin(MyVector<User*>& vct, User *user, int bitCoinId) {
    MyVector<Bitcoin*> *bitCoinList;
    for(int i=0; i<vct.size(); i++) {
        bitCoinList = vct[i]->getBitCoinList();
        for(int j=0; j<bitCoinList->size(); j++) {
            if((*bitCoinList)[j]->bitCoinId == bitCoinId) {
                return false;
            }
        }
    }
    bitCoinList = user->getBitCoinList();
    for(int i=0; i<bitCoinList->size(); i++) {
        if((*bitCoinList)[i]->bitCoinId == bitCoinId) {
            return false;
        }
    }

    return true;
}

/*
 * Checking if a transaction is valid,enough money 
 * positive amount,etc... If so we make the transaction
 * and update all the appropriate data structures
 */

bool isValidTransaction(MyVector<User*>& vct,Transaction *tr) {
    if(tr->amount <=0 || strcmp(tr->senderId,tr->receiverId) ==0) 
        return false;
    int sendIndex = -1,recIndex = -1;
    for(int i=0; i<vct.size(); i++) {
        if(strcmp(vct[i]->getUserId(),tr->senderId) == 0) {
            if(vct[i]->getBalance() < tr->amount) {
               return false;
            }
            else {
                sendIndex = i;
                if(recIndex >=0) 
                    break;
            }
        }

        else if(strcmp(vct[i]->getUserId(),tr->receiverId) == 0) {
            recIndex = i;
        }
    }
    if(sendIndex >= 0 && recIndex >=0) {
        //Valid transaction, all the other conditions mean that is not valid
        depositBitCoins(vct[sendIndex],vct[recIndex],tr->amount);
        vct[sendIndex]->setBalance(-(tr->amount));
        vct[recIndex]->setBalance(tr->amount);
        return true;
    }
    else {
        return false;
    }       
}

/*
 * That is the function where we send bitCoins from 
 * the transaction sender to the transaction receiver
 */
void depositBitCoins(User* sender,User* receiver,int amount) {
    int index;
    MyVector<Bitcoin*> *senderBitCoins = sender->getBitCoinList(),*receiverBitCoins = receiver->getBitCoinList();
    for(int i=senderBitCoins->size()-1; i>=0; i--) {
        if(amount <= 0)
            return;
        int sendValue = (*senderBitCoins)[i]->currentValue, sendBitCoinId = (*senderBitCoins)[i]->bitCoinId;

        if((index = existsBitCoin(receiverBitCoins,sendBitCoinId)) != -1) {
            if(amount >= sendValue) {
                amount -= sendValue;
                (*receiverBitCoins)[index]->currentValue += sendValue;
                //senderBitCoins->remove(i);
                senderBitCoins->erase(i);
            }
            else {
                (*receiverBitCoins)[index]->currentValue += amount;
                (*senderBitCoins)[i]->currentValue -= amount;
                return ;
            }
        }
        else {
            if(amount >= sendValue) {
                amount -= sendValue;
                //receiverBitCoins->add((*senderBitCoins)[i]);
                //senderBitCoins->remove(i);
                receiverBitCoins->add(new Bitcoin(sendBitCoinId,sendValue));
                senderBitCoins->erase(i);
            }
            else {
                (*senderBitCoins)[i]->currentValue -= amount; 
                receiverBitCoins->add(new Bitcoin(sendBitCoinId,amount));
                return ;
            }
        }
    }
}

/*
 * Checking if a certain user has a bitCoin already
 */
int existsBitCoin(MyVector<Bitcoin*> *vct,int bitCoinId) {
    for(int i=0; i<vct->size(); i++) {
        if((*vct)[i]->bitCoinId == bitCoinId)
            return i;
    }
    return -1;
}

// void printBitCoins(User* user) {
//     MyVector<Bitcoin*> * btList = user->getBitCoinList();
//     for(int i=0; i<btList->size(); i++) {
//         std::cout << "Bitcoin " << (*btList)[i]->bitCoinId << " has value "<< (*btList)[i]->currentValue << std::endl;
//     }
// }

/*
 * Iterating through all users,
 * if we find a user with that userId,then we return his account balance
 */
int walletStatus(MyVector<User*>& vct,char *token) {
    if(strtok(NULL,DELIM) != NULL || token == NULL) {
        std::cerr << "Wrong format of command" << std::endl;
        return -1;
    }
    for(int i=0; i<vct.size(); i++) {
        if(strcmp(vct[i]->getUserId(),token) == 0) {
            //printBitCoins(vct[i]);
            return vct[i]->getBalance();
        }
    }
    std::cerr << "User with userId " << token << " doesn't exist" << std::endl;
    return -1;
    
}
//Get a random integer up to n-1
inline int intN(int n) {
    return rand() % n;
}

//Generate a random string from the alphabet below and with length len
char *randomString(int len,char* rstr) {
  const char alphabet[] = "abcdefghijklmnopqrstuvwxyz0123456789";  
  int i;
  for (i = 0; i < len; i++) {
    rstr[i] = alphabet[intN(strlen(alphabet))];
  }
  rstr[len] = '\0';
  return rstr;
}

//Just comparing 2 dates
bool afterLastDate(struct tm tm1,struct tm tm2) {
    char tm1_buffer[20],tm2_buffer[20];

    // tm1.tm_year -= 1900;
    // tm2.tm_year -= 1900;
    // tm1.tm_mon -= 1;
    // tm2.tm_mon -=1;

    strftime(tm1_buffer,sizeof(tm1_buffer),"%F %R",&tm1);
    strftime(tm2_buffer,sizeof(tm2_buffer),"%F %R",&tm2);

    if(strcmp(tm1_buffer,tm2_buffer) > 0) 
        return true;
    return false;
}


/*
 * That's the function where requestTransaction,requestTransactions are based
 * We just do what we did in main with the transactions from the transactionsFile,
 * with some small changes in parsing and some more constraints
 */
bool requestTransaction(Hash& sender,Hash& receiver,MyVector<Transaction*>& trVct,MyVector<User*>& vct,char* token) {
    if(token == NULL) 
        return false;

    char *senderId,*receiverId,*senderId2,*receiverId2,*trId =(char*) malloc((RAND_LENGTH + 1) * sizeof(char));
    int amount;
    struct tm tmst;
    Transaction *tr;

    while(sender.searchTransaction(randomString(RAND_LENGTH,trId))); 
    // 2 versions of a given transaction,with date or not
    if(sscanf(token,"%ms %ms %d %d-%d-%d %d:%d",&senderId,&receiverId,&amount,&(tmst.tm_mday),&(tmst.tm_mon),&(tmst.tm_year),&(tmst.tm_hour),&(tmst.tm_min)) == 8) {
            tr = new Transaction(trId,senderId,receiverId,amount,tmst);
            if(!afterLastDate(tr->tmstamp,trVct[trVct.size()-1]->tmstamp)){
                printf("Transaction %s %s %s %d %02d-%02d-%d %02d:%02d was unsuccessful\n",tr->transactionId,tr->senderId,tr->receiverId,tr->amount,tr->tmstamp.tm_mday,tr->tmstamp.tm_mon,tr->tmstamp.tm_year,tr->tmstamp.tm_hour,tr->tmstamp.tm_min);
                delete tr;
                return false;
            }
    }
    else {
        if(sscanf(token,"%ms %ms %d",&senderId2,&receiverId2,&amount) == 3) {
            free(senderId);
            free(receiverId);
            time_t now=time(0);
            struct tm* tm1 = localtime(&now);
            tm1->tm_mon++;
            tm1->tm_year += 1900;
            tr = new Transaction(trId,senderId2,receiverId2,amount,*tm1);
        }
        //Unacceptable transaction format
        else {
            std::cout<<"Wrong format of transaction"<<std::endl;
            free(trId);
            free(senderId2);
            free(receiverId2);
            free(senderId);
            free(receiverId);
            return false;
        }
    }
    if(!isValidTransaction(vct,tr)) {
        printf("Transaction %s %s %s %d %02d-%02d-%d %02d:%02d was unsuccessful\n",tr->transactionId,tr->senderId,tr->receiverId,tr->amount,tr->tmstamp.tm_mday,tr->tmstamp.tm_mon,tr->tmstamp.tm_year,tr->tmstamp.tm_hour,tr->tmstamp.tm_min);
        delete tr;
        return false;
    }

    //Update the proper data structures
    trVct.add(tr);
    sender.insertItem(tr->senderId,tr);
    receiver.insertItem(tr->receiverId,tr);
    printf("Transaction %s %s %s %d %02d-%02d-%d %02d:%02d was successful\n",tr->transactionId,tr->senderId,tr->receiverId,tr->amount,tr->tmstamp.tm_mday,tr->tmstamp.tm_mon,tr->tmstamp.tm_year,tr->tmstamp.tm_hour,tr->tmstamp.tm_min);
    return true;
}

//Just parsing input and calling requestTransaction for every transaction
void requestTransactions(Hash& sender,Hash& receiver,MyVector<Transaction*>& trVct,MyVector<User*>& vct,char* token) {
    token = strtok(NULL,TRANSACTION_DELIM);
    if(strchr(token,' ') == NULL) {
        FILE *trFile;
        if((trFile = fopen(token,"r")) == NULL) {
            std::cerr<<"There was a problem opening the file"<<std::endl;
            return;
        };

        ssize_t read; 
        size_t len = 0;
        char *line = NULL;

        while ((read = getline(&line, &len, trFile)) !=-1) {
            line = strtok(line,"\n");
            token = strtok(line,TRANSACTION_DELIM);
            while(token != NULL) {
                requestTransaction(sender,receiver,trVct,vct,token);
                token = strtok(NULL,TRANSACTION_DELIM);
            }
        }
        fclose(trFile);
        free(line);
        
    }
    else {
        while(token!= NULL) {
            requestTransaction(sender,receiver,trVct,vct,token);
            token = strtok(NULL,TRANSACTION_DELIM);
        }
    }

}

/*
 * That's the functions on which findPayments and findEarnings is based on
 * We search for user's transactions and if they are made in a certain time or date period 
 * that is given from the user, then we print them and add their amount to the total paid or earned amount in this period.
 * If no period is given, then all transactions are printed and added to the total paid or earned amount
 */
int findTransactions(Hash& hashTable,char* token) {
    char* vct[5];
    int count = 0,way = 2;
    struct tm tm1={0,0,0,0,0,0,0,0,0},tm2={0,0,0,0,0,0,0,0,0};


    token = strtok(NULL,DELIM);
    while(token != NULL) {
        if(count == 5) {
            std::cerr << "Wrong format of command" <<std::endl;
            return -1;
        }
        vct[count++]=token;
        token = strtok(NULL,DELIM);
    }
    if(count % 2 == 0) {
        std::cerr << "Wrong format of command" << std::endl;
        return -1;
    }

    if(count == 3) {
        if(sscanf(vct[1],"%d:%d",&(tm1.tm_hour),&(tm1.tm_min)) == 2 && sscanf(vct[2],"%d:%d",&(tm2.tm_hour),&(tm2.tm_min)) == 2) {
            way = 0;
        }
        else if(sscanf(vct[1],"%d-%d-%d",&(tm1.tm_mday),&(tm1.tm_mon),&(tm1.tm_year)) == 3 && 
                sscanf(vct[2],"%d-%d-%d",&(tm2.tm_mday),&(tm2.tm_mon),&(tm2.tm_year)) == 3) {
            way = 1;
        }
        else {
            std::cerr << "Wrong format of command" << std::endl;
            return -1;
        }  
    }
    else if(count == 5){
        if(sscanf(vct[1],"%d:%d",&(tm1.tm_hour),&(tm1.tm_min)) != 2 || sscanf(vct[2],"%d-%d-%d",&(tm1.tm_mday),&(tm1.tm_mon),&(tm1.tm_year)) !=3 || 
            sscanf(vct[3],"%d:%d",&(tm2.tm_hour),&(tm2.tm_min)) != 2 || sscanf(vct[4],"%d-%d-%d",&(tm2.tm_mday),&(tm2.tm_mon),&(tm2.tm_year)) != 3) {

            std::cerr << "Wrong format of command" << std::endl;
            return -1;
        }
    }
    int sum = 0;
    LinkedList<Transaction*> *lst = hashTable.getUserIdTransactions(vct[0]);
    if(lst == NULL) 
        return sum;

    Node<Transaction*> *tr = lst->begin();
    while(tr != NULL) {
        if(count == 1) {
            sum += tr->data->amount;
            printf("%s %s %s %d %02d-%02d-%d %02d:%02d\n",tr->data->transactionId,tr->data->senderId,tr->data->receiverId,tr->data->amount,tr->data->tmstamp.tm_mday,tr->data->tmstamp.tm_mon,tr->data->tmstamp.tm_year,tr->data->tmstamp.tm_hour,tr->data->tmstamp.tm_min);
        }
        else if(isInPeriod(tm1,tm2,tr->data->tmstamp,way)) {
           sum += tr->data->amount;
           printf("%s %s %s %d %02d-%02d-%d %02d:%02d\n",tr->data->transactionId,tr->data->senderId,tr->data->receiverId,tr->data->amount,tr->data->tmstamp.tm_mday,tr->data->tmstamp.tm_mon,tr->data->tmstamp.tm_year,tr->data->tmstamp.tm_hour,tr->data->tmstamp.tm_min);
        }
        tr = tr->next;
    }
    std::cout<<"Total amount is "<<sum<<"$"<<std::endl;
    return sum;
}


/*
 * Just deciding if a date is inbetween 2 other dates
 */
bool isInPeriod(struct tm tm1,struct tm tm2,struct tm tr,int way) {
    // tm1.tm_year -= 1900;
    // tm2.tm_year -= 1900;
    // tr.tm_year -= 1900;
    // tm1.tm_mon -= 1;
    // tm2.tm_mon -=1;
    // tr.tm_mon -=1;

    char tm1_buffer[20],tm2_buffer[20],tr_buffer[20];

    if(way == 2) {
        strftime(tm1_buffer,sizeof(tm1_buffer),"%F %R",&tm1);
        strftime(tm2_buffer,sizeof(tm2_buffer),"%F %R",&tm2);
        strftime(tr_buffer,sizeof(tr_buffer),"%F %R",&tr);
    }
    else if(way == 1 ) {
        strftime(tm1_buffer,sizeof(tm1_buffer),"%F",&tm1);
        strftime(tm2_buffer,sizeof(tm2_buffer),"%F",&tm2);
        strftime(tr_buffer,sizeof(tr_buffer),"%F",&tr);
    }
    else {
        strftime(tm1_buffer,sizeof(tm1_buffer),"%R",&tm1);
        strftime(tm2_buffer,sizeof(tm2_buffer),"%R",&tm2);
        strftime(tr_buffer,sizeof(tr_buffer),"%R",&tr);
    }

    if(strcmp(tm1_buffer,tm2_buffer) > 0) 
        swapStrings(tm1_buffer,tm2_buffer);

    if(strcmp(tm1_buffer,tr_buffer) <= 0 && strcmp(tm2_buffer,tr_buffer) >= 0)
        return true;
    return false; 
}
void swapStrings(char* s1,char* s2) {
    int i = 0;
	char temp;
 
    while(s1[i]!='\0'){
        temp = s1[i];
        s1[i] = s2[i];
        s2[i] = temp;
        i++;
    }

}
