#include "../inc/linked_list.hpp"
#include "../inc/bucket.hpp"
#include "../inc/transaction.hpp"
#include "../inc/hash.hpp"
#include <iostream>
#include <cstring>


/*
 * HashTable functionality insert,search and other functions
 * All hashtable functions call the respective ones from the struct bucket
 */

Hash::Hash(int bck,int bucketSize) {
    BUCKET = bck;
    table = new Bucket*[BUCKET];
    for(int i=0; i<BUCKET; i++) {
        table[i] = new Bucket(bucketSize);
    }
}

void Hash::insertItem(char* id,Transaction* tr) {
    int index = hash(id);
    table[index]->insert(id,tr);
}

bool Hash::searchTransaction(char* transactionId) {
    for(int i=0; i<BUCKET; i++) {
        if(table[i]->searchTransaction(transactionId))
            return true;
    }
    return false;
}

LinkedList<Transaction*>* Hash::getUserIdTransactions(char* id) {
    int index = hash(id);
    LinkedList<Transaction*> *lst = table[index]->getUseridTransactions(id);
    return lst;
}

/* As a hash function, I am taking the sum of the characters of the 'string' 
 * and take the remainder from the division with 'BUCKET'
 */
int Hash::hash(char* s) {
    long int hashval=0;
    for (int i=0; i<strlen(s); i++)  
        hashval += (int)s[i];
    return hashval % BUCKET;
}

Hash::~Hash() {
    for(int i=0; i<BUCKET; i++) {
        delete table[i];
    }
    delete []table;
}
