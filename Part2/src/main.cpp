#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <cstring>
#include <signal.h>
#include "../inc/functions.hpp"
#include "../inc/myvector.hpp"


char *mirror_dir,*log_file;
char *clientFileName,*clientFullFileName;
MyVector<int> childProcesses;


int main(int argc, char **argv) {
    int id,buffer_size;
    int option;
    char *common_dir,*input_dir;


    /*
     * Here I am checking/parsing program arguments
     */
    while ((option = getopt(argc,argv, "n:c:i:m:b:l:")) != -1) {
        switch (option) {
            case 'n':
                id = atoi(optarg);
                break;
            case 'c':
                common_dir = optarg;
                break;
            case 'i':
                input_dir = optarg;
                break;
            case 'm':
                mirror_dir = optarg;
                break;
            case 'b':
                buffer_size = atoi(optarg);
                break;
            case 'l':
                log_file = optarg;
                break;
            default:
                break;
        }
    }
    //Condition for wrong arguments
    if(optind < 13) {
        std::cout<<"Wrong or missing arguments"<<std::endl;
        return 1;
    }
    //Checking about inexistence of input directory
    DIR *inputDir = opendir(input_dir);
    if (inputDir) {
        closedir(inputDir);
    }
    else {
        if (ENOENT == errno) 
            std::cout<< "Input directory doesn't exist" << std::endl;
        else 
            std::cout<< "There was a problem opening the input directory" << std::endl;

        return 1;
    }
    

    //Checking about existence of mirror directory and creating it if it doesn't exist
    int dirExists = mkdir(mirror_dir, 0777);
    if(dirExists != 0) {
        if (errno != EEXIST)
            std::cout << "Couldn't create mirror directory" << std::endl;
        else
            std::cout << "Mirror directory already exists" << std::endl;
        
        return 1;
    }

    //Creating common directory if it doesn't exist
    dirExists = mkdir(common_dir,0777);
    if(dirExists != 0 && errno != EEXIST) {
        std::cout << "Couldn't create common directory" << std::endl;
        return 1;
    }
    //Creating client's file to insert into it process id
    int size;
    if ((size=asprintf(&clientFileName,"%d.id",id)) == -1) {
        std::cout << "Couldn't create the filename" << std::endl;
        return 1;
    }

    clientFullFileName = (char*)malloc((strlen(clientFileName)+strlen(common_dir)+2) * sizeof(char));
    sprintf(clientFullFileName,"%s/%s",common_dir,clientFileName);

    //Checking if a client with the same id already exists
    FILE *clientFile;
    if(fileExists(clientFullFileName)) {
        std::cout << "There is already a client with id " << id << std::endl;
        rmdir(mirror_dir);
        free(clientFileName);
        free(clientFullFileName);
        return 1;
    }

    if((clientFile = fopen(clientFullFileName,"w")) == NULL) {
        std::cout << "Couldn't create client's file" << std::endl;
        return 1;
    }

    fprintf(clientFile,"%d",getpid());
    fclose(clientFile);

    int bytesSent=0,bytesReceived=0;
    int filesSent=0,filesReceived=0;


    //Writing initially to logfile
    FILE *lg = fopen(log_file,"w");
    fprintf(lg,"%d %d\n",id,1);
    fprintf(lg,"%d %d\n",bytesSent,bytesReceived);
    fprintf(lg,"%d %d\n",filesSent,filesReceived);
    fclose(lg);


    /**
     * Open common directory to see 'other' clients
     * For each we make 2 child processes to synchronize
     */
    struct dirent *dir;
    DIR *commonDir = opendir(common_dir);

    int wpid,stat_val;

    if(commonDir) {
        while ((dir = readdir(commonDir)) != NULL) {
            if(strcmp(dir->d_name,".") == 0 || strcmp(dir->d_name,"..") == 0 || strcmp(dir->d_name,clientFileName) == 0)
                continue;
            
            //Forking the 2 child processes
            for(int i=0; i<2; i++) {
                pid_t pid=fork();
                if(pid < 0) {
                    perror("Failed to work");
                    return 1;
                }
                //Parent's code
                else if(pid>0) {
                    childProcesses.add(pid);
                }
                //Child code
                else {
                    closedir(commonDir);
                    free(clientFullFileName);
                    free(clientFileName);
                    exit(0);
                }
            }
        }
        closedir(commonDir);
    }

    //Catching the SIGINT and SIGQUIT signals with sighandler
    sigset_t mask1;
    sigfillset(&mask1);
    
    static struct sigaction act;
    act.sa_handler=catchInterrupt;
    act.sa_mask = mask1;

    sigaction(SIGINT,&act,NULL);
    sigaction(SIGQUIT,&act,NULL);


    //Wait for child processes to end
    while((wpid = wait(&stat_val))>0) {
         std::cout<<wpid<<std::endl;
         childProcesses.searchAndDelete(wpid);
    }

    free(clientFullFileName);
    free(clientFileName);

    // for(int i=0; i<childProcesses.size(); i++) {
    //     std::cout << childProcesses[i] << std::endl;
    // }    

}