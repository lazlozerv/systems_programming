#!/bin/bash

# That is the file which generates all the directories,
# files in round robin sequence

readonly FILEDIRMAXLENGTH=8
readonly FILEMAXCONTENT=128000

#That's the function for generating given length alphanumeric strings
randomStrings() {
    st="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    local rand=""
    for ((i=0; i<$1; i++))
    do
        char=$(($RANDOM % ${#st}))
        rand+="${st:$char:1}"
    done
    echo $rand
}

#That's the function for checking if our $2-$4 arguments are numbers initially and positive at the same time
correctArguments() {
    if [ "$#" -ne 1 ]; then
        return 0
    fi

    ## Checking if argument is number and simultaneously positive
    if [[ "$1" =~ ^[0-9]+$ ]]; then
       return 0
    else
        printf "We want arguments 2-4 to be positive numbers\n"
        return 1
    fi

}


if [ "$#" -ne 4 ]; then
    printf "Wrong number of arguments\n"
    exit 0
fi

for input in {2..4}
do
    if ! correctArguments "${!input}" ; then
        exit 1
    fi
done

if ! mkdir -p "$1"; then
    printf "Probably directory's name is invalid"
    exit 1
fi

### Creating directories in round robin sequence
list_dir=($1)
dv=$(($3/$4))
md=$(($3%$4))

for ((i=0; i<$dv; i++)) 
do
    dr="$1"
    for ((j=0; j<$4; j++))
    do
        rnd=$(($RANDOM % $FILEDIRMAXLENGTH + 1))
        result=$(randomStrings "$rnd")
        dr+="/$result"
        mkdir "$dr"
        list_dir+=($dr)
     done   
done

dr="$1"
for ((i=0; i<$md; i++))
do
    rnd=$(($RANDOM % $FILEDIRMAXLENGTH + 1))
    result=$(randomStrings "$rnd")
    dr+="/$result"
    mkdir "$dr"
    list_dir+=($dr)
done

###Creating and sharing uniformly the files to the directories, again with round robin sequence
ln=${#list_dir[@]}
dv=$(($2/$ln))
md=$(($2%$ln))

for ((i=0; i<$dv; i++)) 
do
    for ((j=0; j<$ln; j++))
    do
        rnd=$(($RANDOM % $FILEDIRMAXLENGTH + 1))
        result=$(randomStrings "$rnd")
        cont_rnd=$(($RANDOM %($FILEMAXCONTENT - 1000+1) + 1000))
        content_str=$(randomStrings "$cont_rnd")
        fl="./${list_dir[j]}/$result.txt"
        echo $content_str > $fl 
    done
done

for ((j=0; j<$md; j++))
do
    rnd=$(($RANDOM % $FILEDIRMAXLENGTH + 1))
    result=$(randomStrings "$rnd")
    cont_rnd=$(($RANDOM %($FILEMAXCONTENT - 1000+1) + 1000))
    content_str=$(randomStrings "$cont_rnd")
    fl="./${list_dir[j]}/$result.txt"
    echo $content_str > $fl 
done

### That's the end of the script



