#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

template <class T> class MyVector;
extern char *mirror_dir,*clientFileName,*clientFullFileName,*log_file;
extern MyVector<int> childProcesses;

bool fileExists(const char*);
void catchInterrupt(int);
int rmDirRecursively(const char *dirname);

#endif