#!/bin/bash

### That's the program from where we get the stats
getStats() {

    cnt=-1
    max=-1 #max client id
    min=-1 #min client id

    #Clients stuff
    connectedClients=0 
    stillConnectedClients=0

    #Files stuff
    filesSent=0
    filesReceived=0

    #Bytes stuff
    bytesSent=0
    bytesReceived=0

    list_id=() #list of connected clients

    ### Every logfile has 3 lines: 1st line has the client id and a bool for showing whether he left the system or not, 2nd line has the number of bytes he sent and received
    ### and 3rd line has the number of files he sent and received
    while read inp1 inp2
    do
        ((cnt++))
        if [[ "$cnt%3" -eq 0 ]]; then
            list_id+=($inp1)
            ((connectedClients++))
            stillConnectedClients=$(($stillConnectedClients + $inp2))

            if [ "$cnt" -eq 0 ]; then
                max=$inp1
                min=$inp1
                continue
            fi

            if [ "$inp1" -gt $max ]; then
                max=$inp1
            elif [ "$inp1" -lt $min ]; then
                min=$inp1
            fi   
        elif [[ "$cnt%3" -eq 1 ]]; then
            bytesSent=$(($bytesSent + $inp1))
            bytesReceived=$(($bytesReceived + $inp2))
        else
            filesSent=$(($filesSent + $inp1))
            filesReceived=$(($filesReceived + $inp2))  
        fi
    done

    ### Printing all the stats we got from 'analysing' the logfiles
    echo "Connected clients:$connectedClients"
    echo "${list_id[*]}"
    echo "Min id:$min and max id:$max"
    echo "Bytes sent:$bytesSent and bytes received:$bytesReceived"
    echo "Files sent:$filesSent and files received:$filesReceived"
    echo "Client that left the system:$(($connectedClients - $stillConnectedClients)) "
}


### Here calling the function for making the stats
getStats